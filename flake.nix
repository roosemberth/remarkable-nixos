{
  description = "Remarkable NixOS";
  inputs.nixpkgs.url = "nixpkgs/nixos-20.09";
  inputs.imx_usb_loader = {
    type = "github";
    owner = "reMarkable";
    repo = "imx_usb_loader";
    flake = false;
  };
  inputs.zg_kernel = {
    type = "github";
    owner = "reMarkable";
    repo = "linux";
    flake = false;
  };

  outputs = { self, nixpkgs, imx_usb_loader, zg_kernel }:
  let
    systems = [ "x86_64-linux" "i686-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
  in {
    overlay = final: prev: with final; {
      imx_loader = stdenv.mkDerivation {
        name = "imx_loader";
        version = "0.0.0";
        src = imx_usb_loader;
        nativeBuildInputs = [ pkgconfig libusb1 ];
        makeFlags = [ "prefix=$(out)" ];
        postInstall = ''
          # Don't ship configurations
          rm -fr $out/etc
        '';
      };
      kernel = (import nixpkgs {
        localSystem = system;
        crossSystem = {
          config = "arm-linux-gnueabihf";
          platform = {
            name = "zero-gravitas";
            kernelMajor = "4.9";
            kernelBaseConfig = "zero-gravitas_defconfig";
            kernelDTB = true;
            kernelArch = "arm";
            kernelAutoModules = false;
            kernelPreferBuiltin = true;
            kernelTarget = "zImage";
            gcc.arch = "armv7-a";
            gcc.fpu = "vfpv3";
          };
        };
        config.allowUnsupportedSystem = true;
      }).buildLinux {
        version = "4.9.84-zero-gravitas";
        src = zg_kernel;
        features.efiBootStub = false;
        kernelPatches = [];
        structuredExtraConfig = with lib.kernel; {
          LOCALVERSION = freeform "-zero-gravitas";
          SPI_BITBANG = yes;
          MAC80211 = yes;
          BRCMUTIL = yes;
          BRCMFMAC = yes;
          RTL_CARDS = yes;
          USB_ACM = yes;
          USB_F_ACM = yes;
          USB_U_SERIAL = yes;
          USB_CDC_COMPOSITE = yes;
        };
      };
    };

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs
    );

    defaultPackage = forAllSystems (pkgs: pkgs.imx_loader);

    devShell = forAllSystems (pkgs: with pkgs; mkShell {
      buildInputs = [ imx_loader ];
    });
  };
}
