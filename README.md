# Work repository for NixOS on remarkable

This directory contains a flake with the required tools to work on the
reMarkable tablet and the bare-bones for targeting a NixOS system to the
remarkable.

Current goals:
- Build the remarkable kernel using the nixpkgs kernel infrastructure. (no
modules support) to target the shipped userspace.

This project uses Nix flakes provide reproductible evalutaions.

Because I'm playing with multiple repositories at a time, the gitignore is set
to ignore any directory. This may change in the future.

## How to develop

You may enter a development environment using `nix develop`.

Note that _Nix Flakes_ are an experimental feature and must be enabled before
using.
